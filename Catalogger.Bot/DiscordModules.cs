using Microsoft.Extensions.DependencyInjection;

using Remora.Discord.Gateway.Extensions;

namespace Catalogger.Bot;

public static class DiscordModuleExtension
{
    public static IServiceCollection AddDiscordModules(this IServiceCollection services)
    {
        return services;
    }
}