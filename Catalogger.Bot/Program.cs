﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Serilog;

using Remora.Discord.API.Abstractions.Gateway.Commands;
using Remora.Discord.Gateway;
using Remora.Discord.Gateway.Extensions;
using Remora.Discord.Gateway.Results;
using Remora.Results;

using Catalogger.Core;

namespace Catalogger.Bot;

public class Init
{
    private IServiceProvider _services;
    private ILogger _logger;

    public static async Task Main(string[] args)
    {
        await new Init(args).RunAsync();
    }

    public Init(string[] args)
    {
        _logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Console()
            .CreateLogger();
        _services = GetServices(args);
    }

    private async Task RunAsync()
    {
        var ct = new CancellationTokenSource();
        Console.CancelKeyPress += (sender, eventArgs) =>
        {
            eventArgs.Cancel = true;
            ct.Cancel();
        };

        var client = _services.GetRequiredService<DiscordGatewayClient>();
        var result = await client.RunAsync(ct.Token);

        if (!result.IsSuccess)
        {
            switch (result.Error)
            {
                case ExceptionError exe:
                    _logger.Error(exe.Exception, "Exception during gateway connection: {ExceptionMessage}", exe.Message);

                    break;
                case GatewayWebSocketError:
                case GatewayDiscordError:
                    _logger.Error("Gateway error: {Message}", result.Error.Message);
                    break;
                default:
                    _logger.Error("Unknown error: {Message}", result.Error.Message);
                    break;
            }
        }
    }

    private IServiceProvider GetServices(string[] args) =>
        new ServiceCollection()
            .AddLogging(builder => builder.AddSerilog(_logger))
            .AddSingleton<IConfiguration>(GetConfiguration(args))
            .AddSingleton<CoreConfig>(services => services
                .GetRequiredService<IConfiguration>()
                .GetSection("Core")
                .Get<CoreConfig>())
            .AddSingleton<BotConfig>(services => services
                .GetRequiredService<IConfiguration>()
                .GetSection("Bot")
                .Get<BotConfig>())
            .AddTransient<ILogger>(_ => new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .CreateLogger())
            .AddDiscordGateway(services => services.GetRequiredService<BotConfig>().Token)
            .Configure<DiscordGatewayClientOptions>(g => g.Intents = this.GatewayIntents)
            .AddDiscordModules()
            .BuildServiceProvider();

    private IConfiguration GetConfiguration(string[] args) =>
        new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("catalogger.json", optional: false)
            .AddEnvironmentVariables()
            .AddCommandLine(args)
            .Build();

    private GatewayIntents GatewayIntents =>
        GatewayIntents.Guilds
        | GatewayIntents.GuildMembers
        | GatewayIntents.GuildBans
        | GatewayIntents.GuildEmojisAndStickers
        | GatewayIntents.GuildWebhooks
        | GatewayIntents.GuildInvites
        | GatewayIntents.GuildMessages
        | GatewayIntents.GuildMessageReactions
        | GatewayIntents.DirectMessages
        | GatewayIntents.DirectMessageReactions
        | GatewayIntents.MessageContents;
}
