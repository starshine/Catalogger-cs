﻿namespace Catalogger.Core;

public class CoreConfig
{
    public string Database { get; set; } = String.Empty;
    public string Redis { get; set; } = String.Empty;
    public string AesKey { get; set; } = String.Empty;
    public string? SentryUrl { get; set; }
}
