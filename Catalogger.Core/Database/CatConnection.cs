using System.Data;

using NodaTime;
using Serilog;
using Npgsql;

namespace Catalogger.Core;

class DbConnection : IDbConnection, ICatConnection
{
    public NpgsqlConnection Inner { get; }
    public Guid ConnId { get; }
    private ILogger _logger;
    private bool _hasOpened;
    private bool _hasClosed;
    private Instant _openedTime;

    public DbConnection(NpgsqlConnection conn, ILogger logger)
    {
        Inner = conn;
        ConnId = Guid.NewGuid();
        _logger = logger;
    }

    public Task OpenAsync(CancellationToken ct = default)
    {
        if (_hasOpened) return Inner.OpenAsync(ct);

        _hasOpened = true;
        _openedTime = SystemClock.Instance.GetCurrentInstant();
        _logger.Verbose("Opened database connection {ConnId}", ConnId);
        return Inner.OpenAsync(ct);
    }

    public Task CloseAsync() => Inner.CloseAsync();

    public IDbCommand CreateCommand() => Inner.CreateCommand();

    public void Dispose()
    {
        Inner.Dispose();
        if (_hasClosed) return;

        LogClose();
    }

    public ValueTask DisposeAsync()
    {
        if (_hasClosed) return Inner.DisposeAsync();

        LogClose();
        return Inner.DisposeAsync();
    }

    public IDbTransaction BeginTransaction()
    {
        _logger.Verbose("Beginning transaction on {ConnId}", ConnId);
        return Inner.BeginTransaction();
    }

    public IDbTransaction BeginTransaction(IsolationLevel level)
    {
        _logger.Verbose("Beginning transaction on {ConnId}", ConnId);
        return Inner.BeginTransaction(level);
    }

    public ValueTask<NpgsqlTransaction> BeginTransactionAsync(CancellationToken ct = default)
    {
        return Inner.BeginTransactionAsync(ct);
    }

    public void Open() => throw SyncError(nameof(Open));
    public void Close() => Inner.Close();

    public void ChangeDatabase(string dbName) => Inner.ChangeDatabase(dbName);

    public string ConnectionString
    {
        get => Inner.ConnectionString;
        set => Inner.ConnectionString = value;
    }

    public string Database => Inner.Database!;
    public ConnectionState State => Inner.State;
    public string DataSource => Inner.DataSource;
    public string ServerVersion => Inner.ServerVersion;
    public int ConnectionTimeout => Inner.ConnectionTimeout;


    private static Exception SyncError(string caller) => throw new Exception($"Executed synchronous IDbCommand function {caller}");

    private void LogClose()
    {
        _hasClosed = true;

        var duration = SystemClock.Instance.GetCurrentInstant() - _openedTime;
        _logger.Debug("Closed connection {ConnId}, open for {Duration}", ConnId, duration);
    }
}