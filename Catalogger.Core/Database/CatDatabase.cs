using Serilog;
using Npgsql;
using Dapper;

namespace Catalogger.Core;

class Database : ICatDatabase
{
    private ILogger _logger;
    private string _connString;

    public Database(ILogger logger, CoreConfig config)
    {
        DefaultTypeMap.MatchNamesWithUnderscores = true;
        NpgsqlConnection.GlobalTypeMapper.UseJsonNet();

        _logger = logger.ForContext<Database>();
        _connString = new NpgsqlConnectionStringBuilder(config.Database)
        {
            Timeout = 2,
            MaxPoolSize = 500
        }.ConnectionString;
    }

    public async Task<ICatConnection> Obtain()
    {
        var conn = new DbConnection(new NpgsqlConnection(_connString), _logger);
        await conn.OpenAsync();
        return conn;
    }
}
