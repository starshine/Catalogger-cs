using System.Text;
using System.Security.Cryptography;

namespace Catalogger.Core;

public interface ICrypt
{
    byte[] EncryptString(string text);
    string DecryptToString(byte[] src);
}

public class Crypt: ICrypt
{
    private const int _nonceSize = 12;
    private const int _tagSize = 16;
    private byte[] _key;

    public Crypt(CoreConfig config)
    {
        _key = Encoding.UTF8.GetBytes(config.AesKey);
    }

    public byte[] EncryptString(string text) => Encrypt(Encoding.UTF8.GetBytes(text));
    public string DecryptToString(byte[] src) => Encoding.UTF8.GetString(Decrypt(src));

    public byte[] Encrypt(byte[] plaintext)
    {
        using var aes = new AesGcm(_key);

        var nonce = RandomNumberGenerator.GetBytes(_nonceSize);
        var tag = RandomNumberGenerator.GetBytes(_tagSize);

        var ciphertext = new byte[plaintext.Length];

        aes.Encrypt(nonce, plaintext, ciphertext, tag);

        return nonce.Concat(ciphertext).Concat(tag).ToArray();
    }

    public byte[] Decrypt(byte[] src)
    {
        using var aes = new AesGcm(_key);

        var nonce = src.Take(_nonceSize).ToArray();
        var tag = src.TakeLast(_tagSize).ToArray();
        var ciphertext = src.Skip(_nonceSize).SkipLast(_tagSize).ToArray();
        var plaintext = new byte[ciphertext.Length];

        aes.Decrypt(nonce, ciphertext, tag, plaintext);

        return plaintext;
    }
}
