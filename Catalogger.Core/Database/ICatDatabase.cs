using System.Data;

namespace Catalogger.Core;

public interface ICatDatabase
{
    Task<ICatConnection> Obtain();
}

public interface ICatConnection : IDbConnection, IAsyncDisposable
{
    Task CloseAsync();
    Task OpenAsync(CancellationToken ct = default);
}